This is a generic interface for logging errors.

### MGI Error Reporter Framework

There is a generic interface that allows extension to log to any datasource, as well as a more concrete implementation that logs to any file.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
